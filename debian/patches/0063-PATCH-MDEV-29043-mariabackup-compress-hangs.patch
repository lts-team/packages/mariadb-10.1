From: Marko Makela <marko.makela@mariadb.com>
Date: Fri, 19 Aug 2022 09:18:24 +0300
Subject: PATCH] MDEV-29043 mariabackup --compress hangs

Even though commit b817afaa1c148437e1016d1981f138d0c46ccbc8 passed
the test mariabackup.compress_qpress, that test turned out to be
too small to reveal one more problem that had previously been prevented
by the existence of ctrl_mutex. I did not realize that there can be
multiple concurrent callers to compress_write(). One of them is the
log copying thread; further callers are data file copying threads
(default: --parallel=1).

By default, there is only one compression worker thread
(--compress-threads=1).

compress_write(): Fix a race condition between threads that would
use the same worker thread object. Make thd->data_avail contain the
thread identifier of the submitter, and add thd->avail_cond to
notify other compress_write() threads that are waiting for a slot.

[backport]
- Convert constructor to cast
- move variable to C89 correct syntax

origin: https://github.com/MariaDB/server/commit/a1055ab35d29437b717e83b1a388eaa02901c42f
---
 extra/mariabackup/ds_compress.c | 54 ++++++++++++++++++++++++++++++++++-------
 1 file changed, 45 insertions(+), 9 deletions(-)

diff --git a/extra/mariabackup/ds_compress.c b/extra/mariabackup/ds_compress.c
index a77f5bd..f257958 100644
--- a/extra/mariabackup/ds_compress.c
+++ b/extra/mariabackup/ds_compress.c
@@ -33,9 +33,10 @@ typedef struct {
 	pthread_t		id;
 	uint			num;
 	pthread_mutex_t		data_mutex;
+	pthread_cond_t  	avail_cond;
 	pthread_cond_t  	data_cond;
 	pthread_cond_t  	done_cond;
-	my_bool			data_avail;
+	pthread_t		data_avail;
 	my_bool			cancelled;
 	const char 		*from;
 	size_t			from_len;
@@ -187,6 +188,9 @@ compress_write(ds_file_t *file, const uchar *buf, size_t len)
 	uint			i;
 	const char		*ptr;
 	ds_file_t		*dest_file;
+	int submitted = 0;
+	int wait;
+	const pthread_t self = pthread_self();
 
 	comp_file = (ds_compress_file_t *) file->ptr;
 	comp_ctxt = comp_file->comp_ctxt;
@@ -197,8 +201,10 @@ compress_write(ds_file_t *file, const uchar *buf, size_t len)
 
 	ptr = (const char *) buf;
 	while (len > 0) {
-		uint max_thread;
-
+		int submitted;
+		int wait = nthreads == 1;
+retry:
+		submitted = 0;
 		/* Send data to worker threads for compression */
 		for (i = 0; i < nthreads; i++) {
 			size_t chunk_len;
@@ -206,16 +212,33 @@ compress_write(ds_file_t *file, const uchar *buf, size_t len)
 			thd = threads + i;
 
 			pthread_mutex_lock(&thd->data_mutex);
+			if (thd->data_avail == (pthread_t)(~0UL)) {
+			} else if (!wait) {
+skip:
+				pthread_mutex_unlock(&thd->data_mutex);
+				continue;
+			} else {
+				for (;;) {
+					pthread_cond_wait(&thd->avail_cond,
+							  &thd->data_mutex);
+					if (thd->data_avail
+					    == (pthread_t)(~0UL)) {
+						break;
+					}
+					goto skip;
+				}
+			}
 
 			chunk_len = (len > COMPRESS_CHUNK_SIZE) ?
 				COMPRESS_CHUNK_SIZE : len;
 			thd->from = ptr;
 			thd->from_len = chunk_len;
 
-			thd->data_avail = TRUE;
+			thd->data_avail = self;
 			pthread_cond_signal(&thd->data_cond);
 			pthread_mutex_unlock(&thd->data_mutex);
 
+			submitted = 1;
 			len -= chunk_len;
 			if (len == 0) {
 				break;
@@ -223,14 +246,21 @@ compress_write(ds_file_t *file, const uchar *buf, size_t len)
 			ptr += chunk_len;
 		}
 
-		max_thread = (i < nthreads) ? i :  nthreads - 1;
+		if (!submitted) {
+			wait = 1;
+			goto retry;
+		}
 
-		/* Reap and stream the compressed data */
-		for (i = 0; i <= max_thread; i++) {
+		for (i = 0; i < nthreads; i++) {
 			int fail = 0;
 			thd = threads + i;
 
 			pthread_mutex_lock(&thd->data_mutex);
+			if (thd->data_avail != self) {
+				pthread_mutex_unlock(&thd->data_mutex);
+				continue;
+			}
+
 			while (!thd->to_len) {
 				pthread_cond_wait(&thd->done_cond,
 						  &thd->data_mutex);
@@ -247,6 +277,8 @@ compress_write(ds_file_t *file, const uchar *buf, size_t len)
 			}
 
 			thd->to_len = 0;
+			thd->data_avail = (pthread_t)(~0UL);
+			pthread_cond_signal(&thd->avail_cond);
 			pthread_mutex_unlock(&thd->data_mutex);
 
 			if (fail) {
@@ -334,6 +366,7 @@ destroy_worker_thread(comp_thread_ctxt_t *thd)
 
 	pthread_join(thd->id, NULL);
 
+	pthread_cond_destroy(&thd->avail_cond);
 	pthread_cond_destroy(&thd->data_cond);
 	pthread_cond_destroy(&thd->done_cond);
 	pthread_mutex_destroy(&thd->data_mutex);
@@ -361,11 +394,14 @@ create_worker_threads(uint n)
 
 		/* Initialize and data mutex and condition var */
 		if (pthread_mutex_init(&thd->data_mutex, NULL) ||
+		    pthread_cond_init(&thd->avail_cond, NULL) ||
 		    pthread_cond_init(&thd->data_cond, NULL) ||
 		    pthread_cond_init(&thd->done_cond, NULL)) {
 			goto err;
 		}
 
+		thd->data_avail = (pthread_t)(~0UL);
+
 		if (pthread_create(&thd->id, NULL, compress_worker_thread_func,
 				   thd)) {
 			msg("compress: pthread_create() failed: "
@@ -407,13 +443,13 @@ compress_worker_thread_func(void *arg)
 	pthread_mutex_lock(&thd->data_mutex);
 
 	while (1) {
-		while (!thd->data_avail && !thd->cancelled) {
+		while (!thd->cancelled
+		       && (thd->to_len || thd->data_avail == (pthread_t)(~0UL))) {
 			pthread_cond_wait(&thd->data_cond, &thd->data_mutex);
 		}
 
 		if (thd->cancelled)
 			break;
-		thd->data_avail = FALSE;
 		thd->to_len = qlz_compress(thd->from, thd->to, thd->from_len,
 					   &thd->state);
 
